package com.sayon.catms.repositories;

import java.util.List;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import com.sayon.catms.models.Cat;

@Repository
public interface CatRepository {
	
	
	
	@Select("select * from tbcat where id=#{id}")
    Cat findOne(@Param("id") Integer id);

	@Select("select * from tbcat")
    List<Cat> findAll();


    @Update("update tbcat set name=#{name}, breed=#{breed}, registry=#{registry}, picture=#{picture} where id=#{id}")
    boolean update(Cat cat);

    @Delete("delete from tbcat where id=#{id}")
    boolean remove(Integer id);


    @Select("select count(*) from tbcat")
    Integer count();

    @Insert("insert into tbcat(name,breed,registry,picture) values(#{name}, #{breed}, #{registry}, #{picture})")
    boolean create(Cat cat);

}
