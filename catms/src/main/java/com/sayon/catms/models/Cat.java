package com.sayon.catms.models;

public class Cat {
    private int id;
    private String name ;
    private String breed;
    private String registry;
    private String picture;

    public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Cat() {
    }

    public Cat(int id, String name, String breed, String registry) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.registry = registry;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getRegistry() {
        return registry;
    }

    public void setRegistry(String registry) {
        this.registry = registry;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", breed='" + breed + '\'' +
                ", registry='" + registry + '\'' +
                '}';
    }
}
