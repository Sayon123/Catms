package com.sayon.catms.services.impl;

import com.sayon.catms.models.Cat;
import com.sayon.catms.repositories.CatRepository;
import com.sayon.catms.services.CatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CatServiceImpl implements CatService {

	
    private CatRepository catRepository;
    
    @Autowired
    public CatServiceImpl(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Override
    public List<Cat> getAll() {
        return this.catRepository.findAll();
    }

    @Override
    public Cat findOne(Integer id) {
        return this.catRepository.findOne(id);
    }

    @Override
    public boolean update(Cat cat) {
        return this.catRepository.update(cat);
    }

    @Override
    public boolean remove(Integer id) {
        return this.catRepository.remove(id);
    }

	@Override
	public void create(Cat cat) {
		this.catRepository.create(cat);
	}
}

