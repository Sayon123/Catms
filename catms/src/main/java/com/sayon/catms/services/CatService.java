package com.sayon.catms.services;

import com.sayon.catms.models.Cat;
import org.hibernate.validator.constraints.URL;

import java.util.List;

public interface CatService {
	void create(Cat cat);
    List<Cat> getAll();
    Cat findOne(Integer id);
    boolean update(Cat cat);
    boolean remove (Integer id);
}
