package com.sayon.catms.controllers;

import com.sayon.catms.models.Cat;
import com.sayon.catms.services.CatService;
import com.sayon.catms.services.UploadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class CatController {

	@Autowired
	private CatService catService;
	
	@Autowired
	private UploadService uploadService;

	@Autowired
	private UploadService uplaodService;

	@RequestMapping(method = RequestMethod.GET, value = { "/index", "/", "/home" })
	public String index(ModelMap model) {
		List<Cat> catList = this.catService.getAll();
		model.addAttribute("cats", catList);
		return "cat/index";
	}

	@GetMapping("/view/{id}")
	public String viewDetail(@PathVariable("id") Integer id, ModelMap model) {
		Cat cat = this.catService.findOne(id);
		model.addAttribute("cat", cat);
		return "cat/view-detail";
	}

	@GetMapping("/update/{id}")
	public String showUpdateForm(@PathVariable Integer id, ModelMap model) {
		Cat cat = this.catService.findOne(id);
		model.addAttribute("cat", cat);
		return "cat/update";
	}

	@PostMapping("/update/submit")
	public String updateSubmit(@RequestParam("file") MultipartFile file,@ModelAttribute("cat") Cat cat) {
		File path = new File("/pp6th");
		
		if (!path.exists())
            path.mkdir();
        String filename = file.getOriginalFilename();

        String extension = filename.substring(filename.lastIndexOf('.') + 1);

        System.out.println(filename);
        System.out.println(extension);


        filename = UUID.randomUUID() + "." + extension;
        System.out.println(filename);
        try {
            Files.copy(file.getInputStream(), Paths.get("/pp6th", filename));
        } catch (IOException e) {

        }

        if (!file.isEmpty()) {
            cat.setPicture("/images-pp/" + filename);
        }

        if (file.isEmpty()){
            String st = this.catService.findOne(cat.getId()).getPicture();
            cat.setPicture(st);
        }

		this.catService.update(cat);
		return "redirect:/index";
	}

	@GetMapping("remove/{id}")
	public String remove(@PathVariable Integer id) {
		this.catService.remove(id);
		return "redirect:/";
	}

	@PostMapping("/cat/add")
	public String save(@RequestParam("file") MultipartFile file, @ModelAttribute("cat") Cat cat) {
		 String filename = this.uploadService.upload(file, "pp/");
	        cat.setPicture("/images-pp/" + filename);
	        System.out.println(cat);
	        this.catService.create(cat);
	        
	        
	        
	
		return "redirect:/";
	}

	@GetMapping("/add")
	public String addCat() {
		return "cat/addcat";
	}

}
