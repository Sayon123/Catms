package com.sayon.catms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CatmsApplication.class, args);
    }
}
