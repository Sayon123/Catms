create table tbcat(
 id serial primary key,
 name varchar,
 breed varchar,
 registry varchar,
 picture varchar
);